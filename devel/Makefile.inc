# $OpenBSD$

SUBDIR += depchk
SUBDIR += flake8
SUBDIR += praw
SUBDIR += prawcore
SUBDIR += py-arrow
SUBDIR += py-codestyle
SUBDIR += py-setuptools
SUBDIR += py-tox
SUBDIR += py-update_checker
SUBDIR += pyflakes
