## Contributing

### General
- **Important** Follow all official porting processes and recommendations found at https://www.openbsd.org/faq/ports/index.html
- Create one branch per port to keep merge requests clean, dependencies not in the tree can be included in the same merge request
- Add your port to the Makefile.inc in the applicable category if not already present
- Any patches should be also passed back to the upstream project if not specific to the OpenBSD platform
- Reasonably verify no dependencies are broken by an update
- Verify applicable tests pass for the port before submitting, expected errors are acceptable

### Ports from the official tree
- Remove CVS metadata from file headers
- Remove original MAINTAINER as they do **not** endorse or maintain these ports
